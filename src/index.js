import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import GlobalStyles from "./components/global.styles";
import "./fonts.css";

ReactDOM.render(
  <>
    <React.StrictMode>
      <App />
    </React.StrictMode>
    <GlobalStyles />
  </>,
  document.getElementById("root")
);
