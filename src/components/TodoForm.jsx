import React, { useState } from "react";
import styled from "styled-components";

const Form = styled.form`
  display: flex;
  justify-content: space-between;
  margin-bottom: 1rem;
`;

const Button = styled.button`
  margin: 0 0.4rem;
  width: 7rem;
  font-size: 2rem;
`;

const Input = styled.input`
  min-width: 30rem;
  font-size: 2rem;
`;

const TodoForm = ({ addTodo }) => {
  const [value, setValue] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!value) return;
    addTodo(value);
    setValue("");
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Input
        type="text"
        className="input"
        value={value}
        placeholder="Your next task"
        onChange={(e) => setValue(e.target.value)}
      />
      <div>
        <Button onClick={handleSubmit}>Add</Button>
      </div>
    </Form>
  );
};

export default TodoForm;
