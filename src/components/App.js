import React, { useState } from "react";
import TodoItem from "./TodoItem";
import TodoForm from "./TodoForm";
import styled from "styled-components/macro";

const ALL = "all";
const ACTIVE = "active";
const COMPLETED = "completed";

const H1 = styled.h1`
  margin: 1rem;
  font-size: 3rem;
`;

const NumberDiv = styled.div`
  font-size: 1.5rem;
`;

const Wrapper = styled.div`
  background: papayawhip;
  height: 100vh;
  padding: 3rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Footer = styled.div`
  display: flex;
  justify-content: space-around;
`;

const TodoList = styled.div`
  background: #e8e8e8;
  border-radius: 0.4rem;
  max-width: 70rem;
  padding: 0.5rem;
  box-shadow: 0rem 2rem 4rem rgb(0 0 0 / 30%);
`;

const App = () => {
  const [todos, setTodos] = useState([
    { text: "First dummy task", isCompleted: false },
    { text: "Second dummy task", isCompleted: false },
    { text: "Third dummy task", isCompleted: false },
  ]);

  const [displayFlag, setDisplayFlag] = useState("all");

  const changeDispalyFlag = (flag) => setDisplayFlag(flag);

  const clearCompleted = () => {
    const newTodos = [...todos].filter((currTodo) => !currTodo.isCompleted);
    setTodos(newTodos);
  };

  const addTodo = (text) => {
    const newTodos = [...todos, { text }];
    setTodos(newTodos);
  };

  const completeTodo = (index) => {
    const newTodos = [...todos];
    newTodos[index].isCompleted = !newTodos[index].isCompleted;
    setTodos(newTodos);
  };

  const removeTodo = (index) => {
    const newTodos = [...todos];
    newTodos.splice(index, 1);
    setTodos(newTodos);
  };

  const numOfCompletedItems = todos.reduce((acc, curr) => {
    acc = curr.isCompleted ? acc + 1 : acc;
    return acc;
  }, 0);

  const numOfUncompletedItems = todos.reduce((acc, curr) => {
    acc = !curr.isCompleted ? acc + 1 : acc;
    return acc;
  }, 0);

  const numOfUncompletedItemsString = `${numOfUncompletedItems} items left`;

  return (
    <Wrapper>
      <H1>Todo App</H1>
      <TodoList>
        <TodoForm addTodo={addTodo} />
        {todos.map((todo, index) => {
          if (
            displayFlag === ALL ||
            (displayFlag === COMPLETED && todo.isCompleted) ||
            (displayFlag === ACTIVE && !todo.isCompleted)
          )
            return (
              <TodoItem
                key={index}
                index={index}
                todo={todo}
                completeTodo={completeTodo}
                removeTodo={removeTodo}
              />
            );
        })}
        <Footer>
          <NumberDiv>{numOfUncompletedItemsString}</NumberDiv>
          <button onClick={() => setDisplayFlag(ALL)}>All</button>
          <button onClick={() => setDisplayFlag(ACTIVE)}>Active</button>
          <button onClick={() => setDisplayFlag(COMPLETED)}>Completed</button>
          {numOfCompletedItems > 0 ? (
            <button onClick={clearCompleted}>Clear Completed</button>
          ) : null}
        </Footer>
      </TodoList>
    </Wrapper>
  );
};

export default App;
