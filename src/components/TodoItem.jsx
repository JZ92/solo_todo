import React from "react";
import styled from "styled-components/macro";

const TODO = styled.div`
  align-items: center;
  background: #fff;
  border-radius: 0.3rem;
  box-shadow: 0.1rem 0.1rem 0.1rem rgba(0, 0, 0, 0.15);
  display: flex;
  font-size: 2rem;
  justify-content: space-between;
  margin-bottom: 0.6rem;
  padding: 0.3rem 0.1rem;
  text-decoration: ${({ isCompleted }) => (isCompleted ? "line-through" : "")};
`;

const Button = styled.button`
  font-size: 1.5rem;
  border-radius: 0.5rem;
  cursor: pointer;
  padding: 0 0.2rem;
  margin: 0 1rem;
  position: relative;
  top: -0.2rem;
`;

const TodoItem = ({ todo, index, completeTodo, removeTodo }) => {
  return (
    <TODO isCompleted={todo.isCompleted}>
      {todo.text}
      <div>
        <input
          type="checkbox"
          checked={todo.isCompleted}
          onClick={() => completeTodo(index)}
        />
        <Button onClick={() => removeTodo(index)}>x</Button>
      </div>
    </TODO>
  );
};

export default TodoItem;
