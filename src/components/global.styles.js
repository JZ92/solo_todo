import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
    html,
    body {
      height: 100%;
      
    }
    html {
      font-size: 10px;
    }

    * {
    font-family: 'Raleway', sans-serif;
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    }
`;

export default GlobalStyles;
